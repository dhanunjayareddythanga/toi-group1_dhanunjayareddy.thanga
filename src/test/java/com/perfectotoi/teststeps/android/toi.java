package com.perfectotoi.teststeps.android;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;

import com.perfectotoi.pages.HomeTestPage;
import com.perfectotoi.util.utility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class toi extends WebDriverBaseTestPage<WebDriverTestPage> {
	HomeTestPage homePage = new HomeTestPage();

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {

	}

	@QAFTestStep(description = "user open TOI app")
	public void userOpenTOIApp() throws InterruptedException {
		Reporter.log("launch app for andorid.....................................");
		utility.openApp("TOI");
	}

	@QAFTestStep(description = "user scroll down till business")
	public void userScrollDownTillBusiness() throws InterruptedException {
		Reporter.log("scroll up to Business section.....");
		while (!homePage.getSensexSectionHomepage().isPresent()) {
			Reporter.log("in while loop...................");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "down");
			js.executeScript("mobile:scroll", scrollObject);
			Thread.sleep(8000);
			Reporter.log("scroll down completed.....................");
		}
		Reporter.log("scrolling successful........"
				+ homePage.getSensexSectionHomepage().getText());
	}

	@QAFTestStep(description = "user opens navigation bar")
	public void userOpensNavigationBar() {
		homePage.getNaivgationbarHomepage().click();
		Reporter.log("Navigation bar is opened..............");
	}

	@QAFTestStep(description = "user get all tab list")
	public void userGetAllTabList() {

		List<QAFWebElement> list = homePage.getListTabHomepage();
		for (int i = 0; i < list.size(); i++) {
			Reporter.log("tab name is......" + list.get(i).getText());

		}
	}

	public QAFExtendedWebElement creatElement(String loc, String key) {

		return new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
	}

	@QAFTestStep(description = "user opens the application")
	public void userOpensTheApplication() throws InterruptedException {
		userOpenTOIApp();
	}

	@QAFTestStep(description = "user clicks on {0}")
	public void userClicksOn(String btn) {
		// CommonStep.waitForPresent("contentdesc.all");
		CommonStep.click(String.format(
				ConfigurationManager.getBundle().getString("contentdesc.all"), btn));

	}

	@QAFTestStep(description = "user clicks on {0} button")
	public void Small(String btn) {
		CommonStep.click(String
				.format(ConfigurationManager.getBundle().getString("text.all"), btn));
	}

	@QAFTestStep(description = "user search for {0} and text is {1}")
	public void userSearchFor(String loc) {

		new QAFExtendedWebElement(loc).sendKeys("");

	}

	@QAFTestStep(description = "user clicks on {0} button and send character {1}")
	public void userClicksOnButtonAndSendCharacter(String btn, String text) {
		String loc = String
				.format(ConfigurationManager.getBundle().getString("text.all", btn));
		CommonStep.sendKeys(text, loc);

	}
	
	@QAFTestStep(description = "user navigates to settings page {0}")
	public void userNavigatesToPage(String data) {
		QAFExtendedWebElement btnSetting =
				new QAFExtendedWebElement("btn.settings.homepage");
		try {
			btnSetting.waitForVisible();
			btnSetting.isDisplayed();
			btnSetting.click();
		} catch (Exception e) {
			QAFExtendedWebElement btnmoreOptions =
					new QAFExtendedWebElement("btn.moreoptions.homepage");
			btnmoreOptions.click();
			QAFExtendedWebElement txtSetting = new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("text.contains.common"),
					data));
			txtSetting.click();
		}
	}
	@QAFTestStep(description = "user click on {0}")
	public void userClickOn(String loc) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(loc);
		element.waitForVisible();
		element.click();
		Reporter.logWithScreenShot("click on register/loginbutton button successfully",
				MessageTypes.Pass);

	}


	@QAFTestStep(description = "user should navigates to profile screen")
	public void userShouldNavigatesToProfileScreen() {

		QAFExtendedWebElement element = new QAFExtendedWebElement("text.city.homepage");
		element.waitForVisible();
		element.isDisplayed();
		Reporter.logWithScreenShot("user navigated to profile page successfully",
				MessageTypes.Pass);

	}
	@QAFTestStep(description = "user click on logout {0} {1}")
	public void userClickOnLogout(String data, String loc) {
		Map<String, Object> params = new HashMap<>();
		params.put("end", "60%,40%");
		params.put("start", "40%,10%");
		params.put("duration", "5");
		Object res = driver.executeScript("mobile:touch:swipe", params);
		QAFExtendedWebElement element = new QAFExtendedWebElement("text.logout.homepage");
		element.click();
		QAFExtendedWebElement element1 =
				new QAFExtendedWebElement("text.Yes.logoutconfirmation");
		element1.click();
		Reporter.logWithScreenShot("logout successfully performed", MessageTypes.Pass);

	}

	@QAFTestStep(description = "user login to application")
	public void userLoginToApplication() {
		CommonStep.clear("txt.email.homepage");
		CommonStep.sendKeys("datsdhana@gmail.com", "txt.email.homepage");
		CommonStep.sendKeys("D@tsdhana1", "txt.password.homepage");
		QAFExtendedWebElement element = new QAFExtendedWebElement("btn.login.homepage");
		element.click();
		Reporter.logWithScreenShot("login performed successfully", MessageTypes.Pass);

	}

	@QAFTestStep(description = "Clicks on {0}")
	public void clicskOn(String menuName) {
		HomeTestPage homePage = new HomeTestPage();
     
		if(menuName.equalsIgnoreCase("story")) {
			homePage.getStory().click();
			Reporter.logWithScreenShot("Successfully Clicked on Story",
					MessageTypes.Pass);	
		}
		
		else if(menuName.equalsIgnoreCase("star")) { 
			homePage.getStar().click();
			Reporter.logWithScreenShot("Successfully Clicked on Start Icon",
					MessageTypes.Pass);
		}
		
		else if(menuName.equalsIgnoreCase("Back")) {
			homePage.getBack_button().click();
			Reporter.logWithScreenShot("Successfully Clicked on Back",
					MessageTypes.Pass);
		}
		
		else if(menuName.equalsIgnoreCase("menu")) {
			homePage.getMenu_icon().click();	
			Reporter.logWithScreenShot("Successfully Clicked on Menu Items",
					MessageTypes.Pass);
		}
		
		else if(menuName.equalsIgnoreCase("Saved Stories")) {
			homePage.getSaved_stories_icon().click();
			Reporter.logWithScreenShot("Successfully Clicked on Saved Stories",
					MessageTypes.Pass);
		}
	}
	
	 @QAFTestStep(description = "verify saved story")
	 public void verifySavedStory() {
		HomeTestPage homePage=new HomeTestPage();
		homePage.getSaved_story().isPresent();
		Reporter.logWithScreenShot("Successfully Verified Saved Story",
				MessageTypes.Pass);
	 }


}
