package com.perfectotoi.pages;

import java.util.List;

//import com.flipkartapptest.components.ProductlistcomponentTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class EntertainmentTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@FindBy(locator = "list.bollywood")
	private List<QAFWebElement> productlist;
	
	@FindBy(locator = "fisrtMovie.bollywood")
	private QAFWebElement fisrtMovieBollywood;
	
	@FindBy(locator = "movieName.bollywood")
	private QAFWebElement movieNameBollywood;
	
	@FindBy(locator = "cast.bollywood")
	private  QAFWebElement castBollywood;
	
	@FindBy(locator = "director.bollywood")
	private QAFWebElement directorBollywood;
	
	public QAFWebElement getFisrtMovieBollywood() {
		return fisrtMovieBollywood;
	}

	public  QAFWebElement getMovieNameBollywood() {
		return movieNameBollywood;
	}

	public QAFWebElement getCastBollywood() {
		return castBollywood;
	}

	public QAFWebElement getDirectorBollywood() {
		return directorBollywood;
	}

	public List<QAFWebElement> getProductlist() {
		return productlist;
	}

	protected void openEntertainmentPage(PageLocator locator, Object... args) {
	}

	@QAFTestStep(description = "user print the list of movie reviews")
	public void userPrintTheListOfMovieReviews() {
		waitForPageToLoad();
		for (int i = 0; i < getProductlist().size(); i++) {
			
			Reporter.log("Movie name :   " + getProductlist().get(i).getText());
		}

	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}
	@QAFTestStep(description="user select first movie from the list")
	public void selectFirstmovieInTheList()
	{
		fisrtMovieBollywood.click();
		
	}
	
	@QAFTestStep(description ="user verify the first movie details in the list")
	public void verifyFirstMovieDetailsInList()
	{
		String MovieName=movieNameBollywood.getText();
		String cast=castBollywood.getText();
		String director=directorBollywood.getText();
		
		Reporter.log("Movie Name : "+MovieName+ " Cast : " +cast+ "Director : "+director);
	}

}
