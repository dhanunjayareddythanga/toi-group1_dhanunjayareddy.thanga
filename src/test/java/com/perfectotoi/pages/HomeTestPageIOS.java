package com.perfectotoi.pages;

import java.util.List;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPageIOS extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "naivgationbar.homepage.ios")
	private QAFWebElement naivgationbarHomepageIos;
	@FindBy(locator = "list.tab.homepage.ios")
	private List<QAFWebElement> listTabHomepageIos;
	@FindBy(locator = "contentdesc.all.ios")
	private QAFWebElement contentdescAllIos;
	@FindBy(locator = "story.text")
	private QAFWebElement story;
	@FindBy(locator = "bookmark.icon")
	private QAFWebElement bookmark;
	@FindBy(locator = "menu.icon")
	private QAFWebElement menu_icon;
	@FindBy(locator = "saved_stories_icon")
	private QAFWebElement saved_stories_icon;
	@FindBy(locator = "back.button")
	private QAFWebElement back_button;
	@FindBy(locator = "saved_story.text")
	private QAFWebElement saved_story;

	public QAFWebElement getStory() {
		return story;
	}

	public QAFWebElement getBookmark() {
		return bookmark;
	}

	public QAFWebElement getMenu_icon() {
		return menu_icon;
	}

	public QAFWebElement getSaved_stories_icon() {
		return saved_stories_icon;
	}

	public QAFWebElement getBack_button() {
		return back_button;
	}

	public QAFWebElement getSaved_story() {
		return saved_story;
	}

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getNaivgationbarHomepageIos() {
		return naivgationbarHomepageIos;
	}

	public List<QAFWebElement> getListTabHomepageIos() {
		return listTabHomepageIos;
	}

	public QAFWebElement getContentdescAllIos() {
		return contentdescAllIos;
	}

}
